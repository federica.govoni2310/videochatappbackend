﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoChatApp.Models;

namespace VideoChatApp.Data.EFCore
{
    public abstract class VideoChatRepository<TEntity, TContext> : IvideoChatRepository<TEntity>
        where TEntity : class, IEntity
        where TContext : VideoChatContext

    {

        private readonly TContext context;
        public VideoChatRepository(TContext context)
        {
            this.context = context;
        }


        public async Task<TEntity> Add(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> Delete(int Id)
        {
            var entity = await context.Set<TEntity>().FindAsync(Id);
            if (entity == null)
            {
                return entity;
            }
            context.Set<TEntity>().Remove(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> Get(int Id)
        {
            return await context.Set<TEntity>().FindAsync(Id);

        }

        public async Task<List<TEntity>> GetAll()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return entity;
        }
    }
}
