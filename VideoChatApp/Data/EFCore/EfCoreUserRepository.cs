﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoChatApp.Models;

namespace VideoChatApp.Data.EFCore
{
    public class EfCoreUserRepository : VideoChatRepository<Registeration, VideoChatContext>
    {
        public EfCoreUserRepository(VideoChatContext context): base(context)
        {

        }
        // We can add new methods specific to the movie repository here in the future
    }
}
