﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoChatApp.Data
{
    public interface IvideoChatRepository<T> where T: class, IEntity
    {
        Task<List<T>> GetAll();
        Task<T> Get(int Id);
        Task<T> Add(T entity);
        Task<T> Update(T entity);
        Task<T> Delete(int Id);
    }
}
