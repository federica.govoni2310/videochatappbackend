﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VideoChatApp.Data.EFCore;
using VideoChatApp.Models;

namespace VideoChatApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : VideoChatController<Registeration, EfCoreUserRepository>
    {
        public UserController(EfCoreUserRepository repository): base(repository){
            
        }
    }
}