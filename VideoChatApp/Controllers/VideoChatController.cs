﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VideoChatApp.Data;

namespace VideoChatApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideoChatController<TEntity, TRepository>: ControllerBase
    
        where TEntity: class, IEntity
        where TRepository: IvideoChatRepository<TEntity>
    
    {
        private readonly TRepository repository;
        public VideoChatController(TRepository repository)
        {
            this.repository = repository;
        }

        // GET: api/VideoChat
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            return await repository.GetAll();
        }

        // GET: api/VideoChat/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<TEntity>> Get(int id)
        {
            var user = await repository.Get(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        // POST: api/VideoChat
        [HttpPost]
        public async Task<ActionResult<TEntity>> Post([FromForm] TEntity user)
        {
            await repository.Add(user);
            return CreatedAtAction("Get", new { id = user.Id }, user);
        }

        // PUT: api/VideoChat/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromForm] TEntity user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }
            await repository.Update(user);
            return NoContent();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TEntity>> Delete(int id)
        {
            var user = await repository.Delete(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }
    }
}
