﻿using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoChatApp.Models
{
    public class VideoChatContext : DbContext
    {
        public VideoChatContext(DbContextOptions<VideoChatContext> options) : base(options) {
        }
        public DbSet<Registeration> Registerations{get; set;}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Initialize();
            //SetupRegisteration(modelBuilder);
        }

        //private void SetupRegisteration(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Registeration>()
        //        .HasKey(x => x.Id);

        //    modelBuilder.Entity<Registeration>()
        //        .HasIndex(u => u.Email)
        //        .IsUnique(true);

        //    modelBuilder.Entity<Registeration>()
        //        .Property(u => u.Email)
        //        .IsRequired()
        //        .HasMaxLength(255);

        //    modelBuilder.Entity<Registeration>()
        //        .Property(u => u.Password)
        //        .IsRequired()
        //        .HasMaxLength(255);

        //    modelBuilder.Entity<Registeration>()
        //        .Property(u => u.UserName)
        //        .IsRequired()
        //        .HasMaxLength(50);

        //}

    }
}
