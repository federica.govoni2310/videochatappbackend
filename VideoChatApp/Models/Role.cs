﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoChatApp.Models
{
    public enum Role
    {
        Admin = 0,
        User = 1
    }
}
