﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoChatApp.Models
{
    public static class DbInitializer
    {
        //public static void Initialize(VideoChatContext context)
        //{
        //    context.Database.EnsureCreated();
        //    if (context.Registerations.Any())
        //    {
        //        return;
        //    }

        //    var user = new Registeration
        //    {
        //        Id = Guid.NewGuid(),
        //        Email = "email@email.com",
        //        UserName = "User1",
        //        Password = "passwrod",
        //        Role = Role.Admin

        //    };
        //}
        public static void Initialize(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Registeration>().HasData(
                new Registeration
                {
                    Id = 1,
                    Email = "email@email.com",
                    UserName = "User1",
                    Password = "passwrod",
                    Role = Role.Admin

                }) ;
           
        }
    }
   
}
